#**************************************************
# implementation
#**************************************************
import pathlib
from luigi.task import flatten

from jinja2 import Template
tmpl_tasks = f'''
import d6tflow
import luigi

# for all tasks in self.tasks
    # for all task in self.traverse(task):

class {{task.name}}({{task.class}}):
    persist={{task.obj.persist}}
    pipename={{self.pipename}}
    external=True
     {% for param in task.params %}
        {{param.name}}={{param.class}}(default={{param.default}})
    {% endfor %}
    
'''


class FlowExport(object):
    
    def __init__(self, tasks, pipename, write_dir='.', write_filename_tasks = 'tasks_d6tpipe.py', write_filename_run = 'run_d6tpipe.py'):
        # todo NN: copy = False # copy task output to pipe
        # todo NN: run = False # run flow
        if not isinstance(tasks, (list,)):
            tasks = [tasks]
        # todo: init all params => self.param=param
        write_dir = pathlib.Path(write_dir)
    
    def traverse(self, t, path=None): # todo NN: move to d6tflow.utils
        if path is None: path = []
        path = path + [t]
        for node in flatten(t.requires()):
            if not node in path:
                path = self.traverse(node, path)
        return path
    
    def generate(self):
        # todo: 
        # for task_ in self.tasks:
            # for task in self.traverse(task_):
            if getattr(task,'export',True):
                taskPrint = {'name':task.__class__.__name__,'class':type(task).__mro__[1],'obj':task}
                taskPrint['params'] = [{'name':param[0], 'class':param[1].__class__,
                 'default': repr(param[1]._default)} for param in task.get_params()]
                tasksPrint.append(taskPrint)

            # todo: how to get class name? type(task).__mro__[1] => d6tflow.tasks.TaskCache BUT str(type(task).__mro__[1]) => Out[52]: "<class 'd6tflow.tasks.TaskCache'>"
            # get d6tflow class if class was inherited: type(task).__mro__.frst('contains','d6tflow.tasks.','d6tflow2.tasks.')

        template = Template(tmpl_tasks)
        template.render(task=tasksPrint[0]) # => todo: save to self.write_dir/self.write_filename_tasks

        tmpl_run = f'''
        import {self.write_filename_tasks[:-3]}
        import d6tflow.pipes
        d6tflow.pipes.init({pipename}) # yes but user can override if they want to save to their own pipe
        d6tflow.pipes.get_pipe({pipename}).pull()

        # to load data see https://d6tflow.readthedocs.io/en/latest/tasks.html#load-output-data
        # available tasks are shown in {self.write_filename_tasks}
        
        '''
        template = tmpl_run # => todo: save to self.write_dir/self.write_filename_run

    def push(self): # pipename.push()
        raise NotImplementedError()
    
    def test(self): # test if target user can run all
        raise NotImplementedError()

    def complete(self): # check if all tasks complete
        raise NotImplementedError()
    
