#**************************************************
# setup
#**************************************************
import d6tflow
import luigi

import pandas as pd
df = pd.DataFrame({'a': range(10)})


class Task1A(d6tflow.tasks.TaskCache):
    persist=['df','df2']
    idx=luigi.IntParameter(default=1)
    idx2=luigi.Parameter(default='test')
    def run(self):
        self.save({'df':df,'df2':df})

class Task1B(d6tflow.tasks.TaskCache):
    persist=['df2','df']
    idx3=luigi.Parameter(default='test3')
    def run(self):
        self.save({'df':df,'df2':df})

@d6tflow.inherits(Task1A,Task1B)
class Task1All(d6tflow.tasks.TaskCache):

    def requires(self):
        return dict([('1',Task1A()),('2',Task1B())])

    def run(self):
        self.save(df)

d6tflow.run(Task1All())
d6tflow.invalidate_upstream(Task1All(), confirm=False)
d6tflow.preview(Task1All())

task = Task1All()

#**************************************************
# tests
#**************************************************

import pytest

from flow_export import FlowExport

class TestExport:

    def test_all(self):

        e = FlowExport(Task1All,'utest-flowexport')
        e.generate()

        code = readfile(e.write_filename_tasks)
        assert code == '''

import d6tflow
        
class Task1A(d6tflow.tasks.TaskCache):
    persist=['df','df2']
    idx=luigi.IntParameter(default=1)
    idx2=luigi.Parameter(default='test')

class Task1B(d6tflow.tasks.TaskCache):
    persist=['df2','df']
    idx3=luigi.Parameter(default='test3')

class Task1All(d6tflow.tasks.TaskCache):
    persist=['data']
    idx=luigi.IntParameter(default=1)
    idx2=luigi.Parameter(default='test')
    idx3=luigi.Parameter(default='test3')
       
        
        '''

        code = readfile(e.write_filename_run)
        assert code == '''

import tasks_d6tpipe
import d6tflow.pipes
d6tflow.pipes.init('utest-flowexport') # yes but user can override if they want to save to their own pipe
d6tflow.pipes.get_pipe('utest-flowexport').pull()


        '''
